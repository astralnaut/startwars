import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharDetailsComponent } from './chars/char-details/char-details.component';
import { CharsComponent } from './chars/chars.component';

const routes: Routes = [
  {
    path: 'characters',
    component: CharsComponent,
    children: [],
  },
  {
    path: 'characters/:id',
    component: CharDetailsComponent,
  },
  {
    path: '',
    redirectTo: 'characters',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
