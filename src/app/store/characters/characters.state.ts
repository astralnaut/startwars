import { Injectable } from '@angular/core';
import { Action, NgxsOnInit, Selector, State, StateContext } from '@ngxs/store';
import { IChar } from 'src/app/models/char.model';
import { CharService } from 'src/app/services/char.service';
import {
  FetchCharacterById,
  FetchCharacters,
  SearchCharacters,
} from './characters.actions';
import { tap } from 'rxjs/operators';

export interface CharactersStateModel {
  loading: boolean;
  canLoadMore: boolean;
  characters: IChar[];
  filterOptions: {
    search?: string;
    page: number;
  };
}

@State<CharactersStateModel>({
  name: 'characters',
  defaults: {
    loading: false,
    canLoadMore: true,
    characters: [],
    filterOptions: {
      page: 1,
    },
  },
})
@Injectable({
  providedIn: 'root',
})
export class CharactersState implements NgxsOnInit {
  constructor(private charactersService: CharService) {}

  ngxsOnInit(ctx: StateContext<CharactersState>): void {
    ctx.dispatch(new FetchCharacters());
  }

  @Selector()
  static canLoadMore(state: CharactersStateModel) {
    return state.canLoadMore;
  }

  @Selector()
  static characters(state: CharactersStateModel) {
    return state.characters;
  }

  @Selector()
  static characterByName(state: CharactersStateModel) {
    return (id: string) => {
      return state.characters.find((char) => char.id === id);
    };
  }

  @Selector()
  static searchQuery(state: CharactersStateModel) {
    return state.filterOptions.search;
  }

  @Selector()
  static loading(state: CharactersStateModel) {
    return state.loading;
  }

  @Action(FetchCharacters)
  fetch(ctx: StateContext<CharactersStateModel>) {
    const state = ctx.getState();
    const page = state.filterOptions.page;
    ctx.patchState({ loading: true });

    return this.charactersService.loadCharacters(page).pipe(
      tap((result) => {
        ctx.patchState({
          loading: false,
          canLoadMore: result.next !== null,
          characters: [
            ...state.characters,
            ...result.results.map(this.addIdToCharacter),
          ],
          filterOptions: {
            page: state.filterOptions.page + 1,
          },
        });
      })
    );
  }

  @Action(FetchCharacterById)
  fetchById(
    ctx: StateContext<CharactersStateModel>,
    action: FetchCharacterById
  ) {
    const chars = ctx.getState().characters;
    ctx.patchState({ loading: true });
    if (chars.find((c) => c.id === action.payload)) {
      return;
    }
    return this.charactersService.loadCharacterById(action.payload).pipe(
      tap((char) => {
        ctx.patchState({
          loading: false,
          characters: [
            ...ctx.getState().characters,
            this.addIdToCharacter(char),
          ],
        });
      })
    );
  }

  @Action(SearchCharacters)
  search(
    { getState, patchState }: StateContext<CharactersStateModel>,
    action: SearchCharacters
  ) {
    const searchQuery = action.payload.trim();
    if (
      action.reset ||
      !getState().filterOptions.search ||
      getState().filterOptions.search !== searchQuery
    ) {
      patchState({
        loading: true,
        characters: [],
        filterOptions: {
          page: 1,
        },
      });
    }
    return this.charactersService
      .searchCharacters(searchQuery, getState().filterOptions.page)
      .pipe(
        tap((result) => {
          patchState({
            loading: false,
            canLoadMore: result.next !== null,
            characters: [
              ...getState().characters,
              ...result.results.map(this.addIdToCharacter),
            ],
            filterOptions: {
              search: searchQuery,
              page: result.next !== null ? getState().filterOptions.page + 1 : 1,
            },
          });
        })
      );
  }

  private addIdToCharacter(char: IChar) {
    const splittedUrl = char.url.split('/');
    return {
      ...char,
      id: splittedUrl[splittedUrl.length - 2],
    };
  }
}
