export class SearchCharacters {
  static readonly type = '[Characters] Search';
  constructor(public payload: string, public reset = false) {}
}

export class LoadMoreCharacters {
  static readonly type = '[Characters] Load More';
  constructor() {}
}

export class FetchCharacters {
  static readonly type = '[Characters] Fetch';
  constructor() {}
}

export class FetchCharacterById {
  static readonly type = '[Characters] Fetch By Id';
  constructor(public payload: string) {}
}
