import { IChar } from './char.model';
import { IFilm } from './film.model';

export interface IPlanet {
  climate: string;
  created: Date;
  diameter: string;
  edited: Date;
  films: string[] | IFilm[];
  gravity: string;
  name: string;
  orbital_period: string;
  population: string;
  residents: string[] | IChar[];
  rotation_period: string;
  surface_water: string;
  terrain: string;
  url: string;
}
