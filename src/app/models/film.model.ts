import { IChar } from './char.model';
import { IPlanet } from './planet.model';
import { ISpecie } from './specie.model';
import { IStarship } from './starship.model';
import { IVehicle } from './vehicle.model';

export interface IFilm {
  characters: string[] | IChar[];
  created: Date;
  director: string;
  edited: Date;
  episode_id: string;
  opening_crawl: string;
  planets: string[] | IPlanet[];
  producer: string;
  release_date: Date;
  species: string[] | ISpecie[];
  starships: string[] | IStarship[];
  title: string;
  url: string;
  vehicles: string[] | IVehicle[];
}
