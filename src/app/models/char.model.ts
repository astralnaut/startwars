import { IFilm } from './film.model';
import { IPlanet } from './planet.model';
import { ISpecie } from './specie.model';
import { IStarship } from './starship.model';
import { IVehicle } from './vehicle.model';

export interface IChar {
  id: string;
  birth_year: string;
  eye_color: string;
  films: string[];
  gender: string;
  hair_color: string;
  height: string;
  homeworld: string | IPlanet;
  mass: string;
  name: string;
  skin_color: string;
  created: Date;
  edited: Date;
  species: string[] | ISpecie[];
  starships: string[] | IStarship[];
  url: string;
  vehicles: string[] | IVehicle[];
}
