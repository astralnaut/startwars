import { Component, Input, OnInit } from '@angular/core';
import { IChar } from 'src/app/models/char.model';

@Component({
  selector: 'app-char',
  templateUrl: './char.component.html',
  styleUrls: ['./char.component.scss']
})
export class CharComponent implements OnInit {
  @Input() char!: IChar;

  constructor() { }

  ngOnInit(): void {
  }

}
