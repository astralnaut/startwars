import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
})
export class SearchInputComponent implements OnInit, AfterViewInit {
  @Output() search = new EventEmitter<string>();
  @Input() canSearch = true;
  @Input() initialSearch = '';
  @ViewChild('searchInput') searchInput!: ElementRef<HTMLInputElement>;

  constructor() {}

  ngAfterViewInit(): void {
    this.searchInput.nativeElement.value = this.initialSearch;
  }

  ngOnInit(): void {
  }

  onSearch(value: string) {
    this.search.next(value);
  }
}
