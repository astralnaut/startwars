import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { concatMap, mergeAll, of, scan } from 'rxjs';
import { IChar } from 'src/app/models/char.model';
import { IFilm } from 'src/app/models/film.model';
import { FetchCharacterById } from 'src/app/store/characters/characters.actions';
import { CharactersState } from 'src/app/store/characters/characters.state';

@Component({
  selector: 'app-char-details',
  templateUrl: './char-details.component.html',
  styleUrls: ['./char-details.component.scss'],
})
export class CharDetailsComponent implements OnInit {
  char?: IChar;
  films: IFilm[] = [];

  constructor(
    private activeRoute: ActivatedRoute,
    private http: HttpClient,
    private router: Router,
    private store: Store,
    private titleService: Title,
  ) {}

  ngOnInit(): void {
    const id = this.activeRoute.snapshot.paramMap.get('id');
    if (!id) {
      this.goToChars();
      return;
    }
    const char = this.store.selectSnapshot(CharactersState.characterByName)(id);
    if (!char) {
      // reload logic
      this.store.dispatch(new FetchCharacterById(id)).subscribe({
        next: () => {
          const char = this.store.selectSnapshot(CharactersState.characterByName)(id);
          this.char = char;
          this.loadCharFilms();
        }
      });
      return;
    }
    this.char = char;

    this.loadCharFilms();
  }

  goToChars() {
    this.router.navigate(['/characters']);
  }

  loadCharFilms() {
    of(this.char!.films)
      .pipe(
        mergeAll(),
        concatMap((film) => this.http.get<IFilm>(film)),
        scan((acc, film) => [...acc, film], [] as IFilm[])
      )
      .subscribe({
        next: (films) => {
          this.films = films;
        },
      });
  }
}
