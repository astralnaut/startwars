import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoCharsComponent } from './no-chars.component';

describe('NoCharsComponent', () => {
  let component: NoCharsComponent;
  let fixture: ComponentFixture<NoCharsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoCharsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoCharsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
