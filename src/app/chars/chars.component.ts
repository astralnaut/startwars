import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { IChar } from '../models/char.model';
import {
  FetchCharacters,
  SearchCharacters,
} from '../store/characters/characters.actions';
import { CharactersState } from '../store/characters/characters.state';

@Component({
  selector: 'app-chars',
  templateUrl: './chars.component.html',
  styleUrls: ['./chars.component.scss'],
})
export class CharsComponent implements OnInit {
  @Select(CharactersState.characters) chars!: Observable<IChar[]>;
  @Select(CharactersState.canLoadMore) canLoadMore!: Observable<boolean>;
  @Select(CharactersState.loading) loading$!: Observable<boolean>;

  private _initialSearch = '';
  get initialSearch() {
    return this._initialSearch;
  }

  constructor(private store: Store, private titleService: Title) {}

  ngOnInit(): void {
    this.titleService.setTitle('Characters List');
    const stateSearch = this.store.selectSnapshot(CharactersState.searchQuery);
    if (stateSearch) {
      this._initialSearch = stateSearch;
    }
  }

  loadMore() {
    const search = this.store.selectSnapshot(CharactersState.searchQuery);
    this.store.dispatch(
      search ? new SearchCharacters(search) : new FetchCharacters()
    );
  }

  search(value: string) {
    const trimmedValue = value.trim();
    if (trimmedValue) {
      this.store.dispatch(new SearchCharacters(trimmedValue, true));
    } else if (this.store.selectSnapshot(CharactersState.searchQuery)) {
      this.store.dispatch(new FetchCharacters());
    }
  }
}
