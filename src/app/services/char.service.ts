import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IChar } from '../models/char.model';
import {
  Observable,
  BehaviorSubject,
  concatMap,
  scan,
  map,
  shareReplay,
} from 'rxjs';

interface ICharResult {
  count: number;
  next: string;
  previous: string;
  results: IChar[];
}

@Injectable({
  providedIn: 'root',
})
export class CharService {
  readonly baseUrl = 'https://swapi.dev/api/people'; // todo: to env
  constructor(private http: HttpClient) {}

  loadCharacterById(payload: string) {
    return this.http.get<IChar>(`${this.baseUrl}/${payload}`);
  }

  loadCharacters(page: number) {
    return this.http.get<ICharResult>(this.baseUrl, {
      params: {
        page,
      },
    });
  }

  searchCharacters(payload: string, page: number) {
    return this.http.get<ICharResult>(this.baseUrl, {
      params: {
        page,
        search: payload,
      },
    });
  }

  // private pageNumber$ = new BehaviorSubject<number>(1);
  // pageNumberAction$ = this.pageNumber$.asObservable();

  // private _canLoadMore = true;
  // get canLoadMore() {
  //   return this._canLoadMore;
  // }

  // chars$ = this.pageNumberAction$.pipe(
  //   concatMap((pageNumber) =>
  //     this.http.get<ICharResult>(`${this.baseUrl}?page=${pageNumber}`).pipe(
  //       map((res) => {
  //         console.log(res.next, res.next === null)
  //         if (res.next === null) {
  //           this._canLoadMore = false;
  //         }
  //         return res.results;
  //       })
  //     )
  //   ),
  //   scan((acc, value) => [...acc, ...value])
  // );

  // constructor(private http: HttpClient) {}

  // nextPage() {
  //   this.pageNumber$.next(this.pageNumber$.value + 1);
  // }

  // resetPage() {
  //   this.pageNumber$.next(1);
  // }
}
