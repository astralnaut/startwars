import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CharsComponent } from './chars/chars.component';
import { SearchInputComponent } from './chars/search-input/search-input.component';
import { CharComponent } from './chars/char/char.component';
import { HttpClientModule } from '@angular/common/http';
import { CharDetailsComponent } from './chars/char-details/char-details.component';
import { TabsComponent } from './tabs/tabs.component';
import { TabComponent } from './tabs/tab/tab.component';
import { ButtonComponent } from './button/button.component';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { environment } from 'src/environments/environment';
import { CharactersState } from './store/characters/characters.state';
import { NoCharsComponent } from './chars/no-chars/no-chars.component';

@NgModule({
  declarations: [
    AppComponent,
    CharsComponent,
    SearchInputComponent,
    CharComponent,
    CharDetailsComponent,
    TabsComponent,
    TabComponent,
    ButtonComponent,
    NoCharsComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgxsModule.forRoot([CharactersState], {
      developmentMode: !environment.production,
    }),
    NgxsReduxDevtoolsPluginModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
